'''This module is the test of CamelCase'''

from works import camel_case
import unittest


class TestCamel(unittest.TestCase):
    """This is code section"""
    def test_give_thisIsMyFirstName_shoud_be_5(self):
        """input thisIsMyFirstName"""
        self.assertEqual(camel_case.camel_case('thisIsMyFirstName'), 5, 'Should be 5')

    def test_give_abcd_shoud_be_1(self):
        """input abcd"""
        self.assertEqual(camel_case.camel_case('abcd'), 1, 'Should be 1')
 
    def test_give_saveChangesInTheEditorSmack_shoud_be_6(self):
        """input saveChangesInTheEditorSmack"""
        self.assertEqual(camel_case.camel_case('saveChangesInTheEditorSmack'), 6, 'Should be 6')

    def test_give_thisIsMethionylThreonylThreonylglutaMinylarginylIsoleucine_shoud_be_7(self):
        """input thisIsMethionylThreonylThreonylglutaMinylarginylIsoleucine"""
        self.assertEqual(camel_case.camel_case('thisIsMethionylThreonylThreonylglutaMinylarginylIsoleucine'), 7, 'Should be 7')

    def test_give_abCd_shoud_be_2(self):
        """input abCd"""
        self.assertEqual(camel_case.camel_case('abCd'), 2, 'Should be 2')
    def test_give_absdDpdfK07aolwKfmslasqefOPPdkascmpfwmpvwl77d45sFfs_shoud_be_8(self):
        """absdDpdfK07aolwKfmslasqefOPPdkascmpfwmpvwl77d45sFfs"""
        self.assertEqual(camel_case.camel_case('absdDpdfK07aolwKfmslasqefOPPdkascmpfwmpvwl77d45sFfs'), 8, 'Should be 8')


if __name__ == '__main__':
     print('testing')
     unittest.main()
     print('Finish!! , No Error')

